import { createWebHistory, createRouter } from "vue-router";
import LoginPages from "@/pages/LoginPages.vue";
import HomePage from '@/pages/HomePage'
import Devices from '@/pages/DevicesPage.vue'
import Wizard from '@/pages/WizardPage.vue'
import axios from 'axios'

const routes = [
  {
    path: '/login',
    name: 'login',
    component: LoginPages,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/',
    name: 'homepage',
    component: HomePage,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/devices',
    name: 'devices',
    component: Devices,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/wizard',
    name: 'wizard',
    component: Wizard,
    meta: {
      requiresAuth: true
    }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('token') == null) {
      next({ name: 'login' })
    } else {
      next()
    }
  } else {
    next()
  }
})

axios.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem('token')
    if(token){
      config.headers['Authorization'] = `Bearer ${ token }`
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default router;